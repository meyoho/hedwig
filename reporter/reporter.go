package reporter

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"sync"
	"time"

	"hedwig/cache"
	"hedwig/common"
	"hedwig/config"
	"hedwig/model"

	"github.com/parnurzeal/gorequest"
	"k8s.io/client-go/pkg/api"
	"k8s.io/client-go/pkg/api/v1"
)

type EventDetail struct {
	Operation   string    `json:"operation"`
	Operator    string    `json:"operator"`
	ClusterUUID string    `json:"cluster_uuid"`
	ClusterName string    `json:"cluster_name"`
	Source      string    `json:"source"`
	Event       *v1.Event `json:"event"`
}

type Event struct {
	ID           string      `json:"id"`
	ResourceType string      `json:"resource_type"`
	ResourceID   string      `json:"resource_id"`
	Time         int64       `json:"time"`
	LogLevel     int         `json:"log_level"`
	Detail       EventDetail `json:"detail,omitempty"`
}

func (evt *Event) String() string {
	return fmt.Sprintf("%s/%s %s", evt.ResourceType, evt.ResourceID, evt.Detail.Operation)
}

type EventReporter struct {
	stopped  bool
	ch       chan *Event
	wg       sync.WaitGroup
	seq      int64
	request  *gorequest.SuperAgent
	debug    bool
	cluster  *model.Cluster
	fetcher  *cache.Fetcher
	updateAt time.Time
	Retry    map[string]*v1.Event
	Logger   *common.Logger
}

func (r *EventReporter) String() string {
	return fmt.Sprintf("{\n  \"cluster\": \"%s/%s\",\n  \"stopped\": %v,\n  \"updated_at\": \"%v\"\n}\n", r.cluster.Name, r.cluster.UUID, r.stopped, r.updateAt)
}

func (r *EventReporter) Stop() {
	if r.stopped {
		return
	}
	r.stopped = true
}

func (r *EventReporter) Report(evt *v1.Event) {
	if r.stopped {
		return
	}
	if evt == nil {
		r.Logger.Error("Event is nil?")
		return
	}
	event := r.Build(evt)
	if event.Time > r.seq {
		r.seq = event.Time
	} else {
		r.seq++
		event.Time = r.seq
	}

	select {
	case r.ch <- event:
	default:
		r.Logger.Infof("Drop event %s as select failed: %s", evt.UID, evt.InvolvedObject)
	}
}

func (r *EventReporter) Build(evt *v1.Event) *Event {
	logLevel := 0
	if evt.Type == api.EventTypeWarning {
		logLevel = 1
	}
	var operator string
	if evt.Source.Component != "" {
		if evt.Source.Host != "" {
			operator = fmt.Sprintf("%s@%s", evt.Source.Component, evt.Source.Host)
		} else {
			operator = evt.Source.Component
		}
	}
	return &Event{
		ID:           string(evt.UID),
		ResourceType: evt.InvolvedObject.Kind,
		ResourceID:   string(evt.InvolvedObject.UID),
		Time:         evt.LastTimestamp.Unix() * 1000000, // in microsecond
		LogLevel:     logLevel,
		Detail: EventDetail{
			Operation:   evt.Reason,
			Operator:    operator,
			Source:      "kubernetes",
			ClusterUUID: r.cluster.UUID,
			ClusterName: r.cluster.Name,
			Event:       evt,
		},
	}
}

func (r *EventReporter) loop() {
	r.wg.Add(1)
	defer r.wg.Done()
	defer close(r.ch)

	var eventQueue []*Event
	retry := 0
	for !r.stopped {
		r.update()
		maxEvent := config.GetInt("max_event_count")
		reportBefore := time.Now().Add(time.Second * time.Duration(config.GetInt("max_event_interval")))
		for len(eventQueue) < maxEvent && time.Now().Before(reportBefore) {
			select {
			case event := <-r.ch:
				if event == nil {
					break
				}
				eventQueue = append(eventQueue, event)
			case <-time.After(reportBefore.Sub(time.Now())):
				break
			}
		}
		if len(eventQueue) == 0 {
			continue
		}
		if err := r.report(eventQueue); err != nil {
			retry++
			if retry < config.GetInt("max_report_retry") {
				time.Sleep(time.Second)
				continue
			}
			r.Logger.Error("Report abort!")
		}
		eventQueue = eventQueue[:0]
		retry = 0
	}

	if len(eventQueue) == 0 {
		return
	}
	r.report(eventQueue)
}

func (r *EventReporter) Join() {
	r.wg.Wait()
}

func (r *EventReporter) report(events []*Event) error {
	body, err := json.Marshal(events)
	if err != nil {
		r.Logger.Error(err.Error())
		return err
	}
	url := fmt.Sprintf("%s%s", config.Get("MORGANS_ENDPOINT"), config.Get("EVENT_CALLBACK_URL"))
	r.Logger.Infof("Try to report %d events to %s", len(events), url)
	resp, _, errs := r.request.Post(url).Send(string(body)).End()
	if len(errs) != 0 {
		r.Logger.Errorf("Report events failed with request error: %s", errs[0])
		return errs[0]
	}
	if resp.StatusCode > 300 {
		r.Logger.Errorf("Report events failed with status code %d", resp.StatusCode)
		return fmt.Errorf("post failed")
	}
	r.Logger.Infof("Report %d events succeeded with status code %d", len(events), resp.StatusCode)
	return nil
}

func (r *EventReporter) update() {
	if time.Now().UTC().Sub(r.updateAt) > 60*time.Second {
		cluster := r.fetcher.Get(r.cluster.UUID)
		if cluster == nil {
			r.stopped = true
			r.Logger.Infof("Update cluster for reporter, reporter will quit")
		}
		r.Logger.Infof("Update cluster for reporter, reporter continue")
		r.updateAt = time.Now().UTC()
		r.cluster = cluster
	}
}

func (r *EventReporter) dial(network, addr string) (net.Conn, error) {
	dial := net.Dialer{
		Timeout:   time.Duration(config.GetInt("MORGANS_TIMEOUT")) * time.Second,
		KeepAlive: 60 * time.Second,
	}
	conn, err := dial.Dial(network, addr)
	if r.debug {
		r.Logger.Infof("Connect done, use ", conn.LocalAddr().String())
	}
	return conn, err
}

func New(cluster *model.Cluster, fetcher *cache.Fetcher) (*EventReporter, error) {
	reporter := &EventReporter{
		stopped:  false,
		ch:       make(chan *Event, config.GetInt("max_event_count")*3),
		debug:    common.LogLevel.String() == "debug",
		cluster:  cluster,
		fetcher:  fetcher,
		updateAt: time.Now().UTC(),
		Logger:   common.NewLogger(map[string]string{"cluster": cluster.Name + "/" + cluster.UUID}),
	}
	reporter.request = gorequest.New().SetDebug(reporter.debug).
		Set("Content-Type", "application/json").
		Set("Authorization", fmt.Sprintf("Token %s", config.Get("token"))).
		Timeout(time.Second * time.Duration(config.GetInt("MORGANS_TIMEOUT")))
	reporter.request.Transport = &http.Transport{Dial: reporter.dial}
	go reporter.loop()
	return reporter, nil
}
