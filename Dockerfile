FROM golang:1.13 as builder
WORKDIR $GOPATH/src/hedwig
COPY . .
RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o $GOPATH/src/hedwig/bin/hedwig .


FROM alpine:3.11
WORKDIR /hedwig

RUN apk add --no-cache supervisor logrotate apk-cron curl

RUN mkdir -p /var/log/mathilde && \
    mkdir -p /etc/cron.hourly/ && \
    mkdir -p /var/log/supervisor/ && \
    rm -rf /etc/cron.daily/*

COPY --from=builder /go/src/hedwig/bin/hedwig ./hedwig
COPY --from=builder /go/src/hedwig/run/supervisord/supervisord.conf /etc/supervisor/supervisord.conf
COPY --from=builder /go/src/hedwig/run/log/logrotate.conf /etc/logrotate.conf
COPY --from=builder /go/src/hedwig/run/log/logrotate.sh /etc/cron.hourly/logrotate

RUN chmod a+x /etc/cron.hourly/logrotate

CMD ["supervisord", "--nodaemon", "--configuration", "/etc/supervisor/supervisord.conf"]
