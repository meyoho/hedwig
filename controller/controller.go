package controller

import (
	"time"

	"hedwig/cache"
	"hedwig/common"
	"hedwig/model"
	"hedwig/provider"
	"hedwig/reporter"
)

type Controller struct {
	Fetcher   *cache.Fetcher
	Logger    *common.Logger
	Reporters []*reporter.EventReporter
	Providers []*provider.EventProvider
}

func New() *Controller {
	return &Controller{
		Fetcher: cache.RunClusterCache(common.NewLogger(map[string]string{"role": "cache"})),
		Logger:  common.NewLogger(map[string]string{"role": "controller"}),
	}
}

func (c *Controller) Run() {
	for {
		time.Sleep(5 * time.Second)
		keys := c.Fetcher.Keys()
		for _, key := range keys {
			cluster := c.Fetcher.Get(key)
			if cluster != nil && !cluster.Cached {
				if err := c.Start(cluster); err != nil {
					c.Logger.Errorf("Start provider&reporter for cluster %s/%s failed.", cluster.Name, cluster.UUID)
					continue
				}
				cluster.Cached = true
				c.Fetcher.Set(key, cluster)
				c.Logger.Infof("Start provider&reporter cluster %s/%s succeeded.", cluster.Name, cluster.UUID)
			}
		}
		time.Sleep(30 * time.Second)
		c.Logger.Infof("Now reporters/providers(%d/%d)", len(c.Reporters), len(c.Providers))

		reporters := ""
		for _, i := range c.Reporters {
			reporters += i.String()
		}
		c.Logger.Debugf("Reporters[%d]:%s", len(c.Reporters), reporters)
		providers := ""
		for _, i := range c.Providers {
			providers += i.String()
		}
		c.Logger.Debugf("Providers[%d]:%s", len(c.Providers), providers)
	}
}

func (c *Controller) Start(cluster *model.Cluster) error {
	r, err := reporter.New(cluster, c.Fetcher)
	if err != nil {
		c.Logger.Errorf("Create reporter for cluster %s/%s failed.", cluster.Name, cluster.UUID)
		return err
	}
	p, err := provider.New(cluster, c.Fetcher)
	if err != nil {
		c.Logger.Errorf("Create provider for cluster %s/%s failed.", cluster.Name, cluster.UUID)
		return err
	}

	go func() {
		for evt := range p.ResultChan() {
			r.Report(evt)
		}
	}()

	c.Reporters = append(c.Reporters, r)
	c.Providers = append(c.Providers, p)
	return nil
}
