package model

const (
	// Common api version
	AVKubernetesV1           = "v1"
	AVClusterV1alpha1        = "clusterregistry.k8s.io/v1alpha1"
	AVInfrastructureV1alpha1 = "infrastructure.alauda.io/v1alpha1"

	// Common resource kind for aiops
	RKClusterRegistry = "Cluster"
	RKClusterFeature  = "Feature"
)
