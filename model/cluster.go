package model

import (
	"encoding/json"
	"reflect"
	"time"

	"hedwig/config"

	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/pkg/api"
	"k8s.io/client-go/rest"
)

type PrometheusConfig struct {
	Endpoint        string `json:"endpoint"`
	Namespace       string `json:"namespace"`
	Name            string `json:"name"`
	Timeout         int    `json:"timeout"`
	IntegrationUUID string `json:"integration_uuid"`
}

type KubernetesConfig struct {
	Endpoint string `json:"endpoint"`
	Token    string `json:"token"`
	Timeout  int    `json:"timeout"`
}

type Cluster struct {
	UUID             string            `json:"uuid"`
	Name             string            `json:"name"`
	IsGlobal         bool              `json:"is_global"`
	Cached           bool              `json:"cached"`
	HasMetricFeature bool              `json:"has_metric_feature"`
	KubernetesConfig *KubernetesConfig `json:"kubernetes_config"`
	PrometheusConfig *PrometheusConfig `json:"prometheus_config"`
	CreatedAt        time.Time         `json:"created_at"`
	UpdatedAt        time.Time         `json:"updated_at"`
}

func (r *Cluster) GenerateRestConfig() *rest.Config {
	cf := &rest.Config{
		Host:        r.KubernetesConfig.Endpoint,
		BearerToken: r.KubernetesConfig.Token,
		Timeout:     time.Duration(config.GlobalConfig.Kubernetes.WatchTimeout) * time.Second,
	}

	if cf.APIPath == "" {
		cf.APIPath = "/api"
	}
	if cf.GroupVersion == nil {
		cf.GroupVersion = &schema.GroupVersion{}
	}
	if cf.NegotiatedSerializer == nil {
		cf.NegotiatedSerializer = api.Codecs
	}
	cf.Insecure = true
	return cf
}


func (r *Cluster) Equal(c *Cluster) bool {
	if r.UUID != c.UUID || r.Name != c.Name {
		return false
	}
	if r.IsGlobal != c.IsGlobal || r.HasMetricFeature != c.HasMetricFeature {
		return false
	}
	if !reflect.DeepEqual(r.KubernetesConfig, c.KubernetesConfig) {
		return false
	}
	return reflect.DeepEqual(r.PrometheusConfig, c.PrometheusConfig)
}

func (r *Cluster) Clone() *Cluster {
	result := &Cluster{}
	data, _ := json.Marshal(r)
	json.Unmarshal(data, result)
	return result
}
