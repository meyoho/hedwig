package common

import (
	"strings"
)

func GetKubernetesTypeFromKind(kind string) string {
	switch strings.ToLower(kind) {
	case "networkpolicy":
		return "networkpolicies"
	case "ingress":
		return "ingresses"
	case "storageclass":
		return "storageclasses"
	case "prometheus":
		return "prometheuses"
	case "endpoints":
		return "endpoints"
	default:
		return strings.ToLower(kind) + "s"
	}
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
