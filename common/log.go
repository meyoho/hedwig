package common

import (
	"os"

	"hedwig/config"

	"github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
)

var LogLevel logrus.Level

func InitLogger() {
	LogLevel, err := logrus.ParseLevel(config.Get("LOG_LEVEL"))
	if err != nil {
		LogLevel = logrus.InfoLevel
	}
	logrus.SetLevel(LogLevel)

	if config.GetBool("LOG_TO_STDOUT") {
		logrus.SetOutput(os.Stdout)
	} else {
		writer := &lumberjack.Logger{
			Filename:   "/var/log/mathilde/hedwig.log",
			MaxSize:    config.GetInt("LOG_SIZE"), // megabytes
			MaxBackups: config.GetInt("LOG_BACKUP"),
			MaxAge:     28,    //days
			Compress:   false, // disabled by default
		}
		logrus.SetOutput(writer)
	}
}

type Logger struct {
	Fields map[string]string
}

func (l *Logger) prefix() string {
	if len(l.Fields) == 0 {
		return ""
	}
	prefix := "["
	for key, value := range l.Fields {
		prefix += key + ":" + value + ","
	}
	slice := []byte(prefix)
	slice = slice[:len(slice)-1]
	prefix = string(slice)
	return prefix + "]"
}

// Infof logs to the INFO log.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Infof(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	logrus.Infof(format, args...)
}

// Info logs to the INFO log.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func (l *Logger) Info(args ...interface{}) {
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	logrus.Info(args...)
}

// Errorf logs to the ERROR, WARNING, and INFO logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Errorf(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	logrus.Errorf(format, args...)
}

// Error logs to the ERROR, WARNING, and INFO logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Error(format string, args ...interface{}) {
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	logrus.Error(args...)
}

// Debug logs to the DEBUG log.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func (l *Logger) Debug(args ...interface{}) {
	if l.prefix() != "" {
		args = append([]interface{}{l.prefix()}, args)
	}
	logrus.Debug(args...)
}

// Debugf logs to the Debug logs.
// Arguments are handled in the manner of fmt.Printf; a newline is appended if missing.
func (l *Logger) Debugf(format string, args ...interface{}) {
	if l.prefix() != "" {
		format = l.prefix() + " " + format
	}
	logrus.Debugf(format, args...)
}

func NewLogger(fields map[string]string) *Logger {
	return &Logger{Fields: fields}
}
