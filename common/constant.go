package common

const (
	// HTTP methods
	HttpGet    = "GET"
	HttpPost   = "POST"
	HttpPut    = "PUT"
	HttpDelete = "DELETE"
	HttpPatch  = "PATCH"
)
