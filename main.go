package main

import (
	"flag"
	"net/http"
	_ "net/http/pprof"

	"hedwig/common"
	"hedwig/config"
	"hedwig/controller"
	"hedwig/metric"

	"github.com/sirupsen/logrus"
)

func main() {
	flag.Parse()
	defer logrus.Info("Service exit.")
	if config.Initialize() != nil {
		logrus.Error("Initialize config failed!")
		return
	}
	common.InitLogger()

	logrus.Info("Service start.")
	go controller.New().Run()

	// add metrics
	http.Handle("/metrics", metric.InstrumentHandler("/metrics"))

	// Start server for debug
	http.ListenAndServe("0.0.0.0:8080", nil)
}
