package furion

import (
	"errors"
	"fmt"
	"time"

	"hedwig/common"
	"hedwig/config"
	"hedwig/model"

	"github.com/Jeffail/gabs"
	"github.com/alauda/bergamot/diagnose"
	"github.com/levigross/grequests"
)

const (
	KubernetesContainerManager = "KUBERNETES"
	// ClusterPlatformV4 support new cluster info and new k8s
	ClusterPlatformV4 = "v4"
)

type Client struct {
	Endpoint   string
	ApiVersion string
	Timeout    int
	Logger     *common.Logger
}

func (c *Client) Diagnose() diagnose.ComponentReport {
	reporter := diagnose.ComponentReport{
		Name:       "furion",
		Status:     "ok",
		Message:    "",
		Suggestion: "",
		Latency:    0,
	}
	url := fmt.Sprintf("%s/_ping", c.Endpoint)
	begin := time.Now()
	c.Logger.Infof("Request furion: GET %s", url)
	response, err := grequests.Get(url, &grequests.RequestOptions{RequestTimeout: time.Duration(c.Timeout) * time.Second})
	c.Logger.Infof("Request furion: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds())
	reporter.Latency = time.Since(begin)
	if err != nil {
		reporter.Message = err.Error()
		reporter.Status = "error"
		c.Logger.Errorf("Request furion error, %s", err.Error())
		return reporter
	}
	if response.StatusCode >= 300 {
		reporter.Message = fmt.Sprintf("furion response code (%d) is more than 300", response.StatusCode)
		reporter.Status = "error"
		c.Logger.Infof("Code %d, Response %s", response.StatusCode, response.String())
		return reporter
	}
	reporter.Message = response.String()
	return reporter
}
func (c *Client) ListClusters() (map[string]*model.Cluster, error) {
	url := fmt.Sprintf("%s/%s/regions", c.Endpoint, c.ApiVersion)
	begin := time.Now()
	c.Logger.Infof("Request furion: GET %s", url)
	response, err := grequests.Get(url, &grequests.RequestOptions{RequestTimeout: time.Duration(c.Timeout) * time.Second})
	c.Logger.Infof("Request furion: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds())
	if err != nil {
		c.Logger.Errorf("Request furion error, %s", err.Error())
		return nil, err
	}
	if response.StatusCode >= 300 {
		c.Logger.Infof("Code %d, Response %s", response.StatusCode, response.String())
		return nil, errors.New(fmt.Sprintf("Furion response code (%d) is more than 300", response.StatusCode))
	}

	clusterList, err := gabs.ParseJSON([]byte(response.String()))
	if err != nil {
		return nil, err
	}

	children, err := clusterList.Children()
	if err != nil {
		return nil, err
	}

	var clusters = map[string]*model.Cluster{}

	for _, child := range children {
		cluster, err := c.generateCluster(child)
		if err != nil {
			c.Logger.Debugf("Generate cluster error, %v", err.Error())
		}
		if cluster != nil {
			clusters[cluster.UUID] = cluster
		}
	}
	return clusters, nil
}

func (c *Client) generateCluster(child *gabs.Container) (*model.Cluster, error) {
	// Generate cluster
	data, err := child.ChildrenMap()
	if err != nil {
		return nil, err
	}
	cluster := &model.Cluster{
		UUID:             data["id"].Data().(string),
		Name:             data["name"].Data().(string),
		KubernetesConfig: &model.KubernetesConfig{},
		PrometheusConfig: &model.PrometheusConfig{},
		CreatedAt:        time.Now().UTC(),
		UpdatedAt:        time.Now().UTC(),
	}

	// Only kubernetes cluster is supported.
	manager, ok := data["container_manager"].Data().(string)
	if !ok {
		return nil, fmt.Errorf("no container manager found for cluster: %s/%s", cluster.Name, cluster.UUID)
	}
	if manager != KubernetesContainerManager {
		return nil, nil
	}

	// Only v3/v4 platform version is supported.
	platform, ok := data["platform_version"].Data().(string)
	if !ok {
		return nil, fmt.Errorf("no platform version found for cluster: %s/%s", cluster.Name, cluster.UUID)
	}
	if platform != ClusterPlatformV4 {
		return nil, nil
	}

	// Generate paths.
	var paths []string
	var values []string
	if platform == ClusterPlatformV4 {
		paths = []string{
			"attr.kubernetes.endpoint",
			"attr.kubernetes.token",
			"features.metric.integration_uuid",
			"features.customized.metric.prometheus_url",
			"features.customized.metric.name",
			"features.customized.metric.namespace",
			"features.customized.metric.prometheus_timeout",
		}
	}

	// Get kubernetes/prometheus config from json.
	ok = false
	body, err := gabs.ParseJSON([]byte(child.String()))
	if err != nil {
		return nil, err
	}
	for _, path := range paths {
		if value, ok := body.Path(path).Data().(string); ok {
			values = append(values, value)
		} else {
			values = append(values, "")
		}
	}
	if values[0] == "" {
		return nil, fmt.Errorf("no %s found for cluster: %s/%s", paths[0], cluster.Name, cluster.UUID)
	}
	if values[1] == "" {
		return nil, fmt.Errorf("no %s found for cluster: %s/%s", paths[1], cluster.Name, cluster.UUID)
	}
	if values[2] == "" && values[3] == "" {
		return nil, fmt.Errorf("no %s && %s found for cluster: %s/%s", paths[2], paths[3], cluster.Name, cluster.UUID)
	}

	// Set kubernetes config and prometheus config for cluster.
	cluster.KubernetesConfig = &model.KubernetesConfig{
		Endpoint: values[0],
		Token:    values[1],
	}
	cluster.PrometheusConfig = &model.PrometheusConfig{
		IntegrationUUID: values[2],
		Endpoint:        values[3],
		Name:            values[4],
		Namespace:       values[5],
	}
	return cluster, nil
}

func NewClient(logger *common.Logger) *Client {
	return &Client{
		Endpoint:   config.GlobalConfig.Furion.Endpoint,
		ApiVersion: config.GlobalConfig.Furion.ApiVersion,
		Timeout:    config.GlobalConfig.Furion.Timeout,
		Logger:     logger,
	}
}
