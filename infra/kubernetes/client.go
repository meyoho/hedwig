package kubernetes

import (
	"encoding/json"
	"fmt"
	"time"

	"hedwig/common"
	"hedwig/model"

	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type Client struct {
	Cluster  *model.Cluster
	REST     *rest.RESTClient
	Resource Resource
	Logger   *common.Logger
}

type Resource interface {
	GetGroup() string
	GetType() string
	GetName() string
	GetSubType() string
	GetNamespace() string
	GetPatchType() string
}

func NewClient(cluster *model.Cluster, resource Resource, logger *common.Logger) *Client {
	cfg := &rest.Config{
		Host:        cluster.KubernetesConfig.Endpoint,
		BearerToken: cluster.KubernetesConfig.Token,
		Timeout:     time.Second * time.Duration(cluster.KubernetesConfig.Timeout),
	}
	cfg.Insecure = true

	cfg.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: scheme.Codecs}
	gv, err := schema.ParseGroupVersion(resource.GetGroup())
	if err != nil {
		logger.Errorf("Get group version error, %s, panic", err.Error())
		panic(err.Error())
	}
	cfg.GroupVersion = &gv
	switch gv.Group {
	case "":
		cfg.APIPath = "/api"
	default:
		cfg.APIPath = "/apis"
	}
	client, err := rest.RESTClientFor(cfg)
	if err != nil {
		logger.Errorf("Get rest client error, %s, panic", err.Error())
		panic(err.Error())
	}
	return &Client{
		Cluster:  cluster,
		REST:     client,
		Resource: resource,
		Logger:   logger,
	}
}

func (c *Client) Request(method string, resource Resource, params map[string]string) (*rest.Result, error) {
	c.Resource = resource
	var request *rest.Request
	switch method {
	case common.HttpPost:
		request = c.PostRequest()
	case common.HttpDelete:
		request = c.DeleteRequest()
	case common.HttpPut:
		request = c.PutRequest()
	case common.HttpGet:
		request = c.GetRequest()
	case common.HttpPatch:
		request = c.PatchRequest()
	default:
		return nil, errors.New(fmt.Sprintf("Unsupported request method: %s", method))
	}

	if params != nil {
		for key, value := range params {
			request = request.Param(key, value)
		}
	}

	request.NamespaceIfScoped(resource.GetNamespace(), c.NamespaceScoped())

	begin := time.Now()
	c.Logger.Infof("Request kubernetes: %s %s", method, request.URL().String())
	result := request.Do()
	if result.Error() != nil {
		c.Logger.Infof("Request kubernetes: Error %s, Latency %.6fs", result.Error().Error(), time.Since(begin).Seconds())
	} else {
		var code int
		result.StatusCode(&code)
		c.Logger.Infof("Request kubernetes: Code %d, Latency %.6fs", code, time.Since(begin).Seconds())
	}
	return &result, result.Error()
}

func (c *Client) getBody() []byte {
	body, err := json.Marshal(c.Resource)
	if err != nil {
		panic(err.Error())
	}
	c.Logger.Infof("Trying to create/update resource with body: %s", string(body))
	return body
}

func (c *Client) NamespaceScoped() bool {
	if c.Resource.GetNamespace() == "" {
		return false
	} else {
		return true
	}
}

func (c *Client) GetRequest() *rest.Request {
	request := c.REST.Get().Resource(c.Resource.GetType()).SubResource(c.Resource.GetSubType())
	if c.Resource.GetName() != "" {
		request.Name(c.Resource.GetName())
	}
	return request
}

func (c *Client) DeleteRequest() *rest.Request {
	request := c.REST.Delete().Resource(c.Resource.GetType()).SubResource(c.Resource.GetSubType())
	if c.Resource.GetName() != "" {
		request.Name(c.Resource.GetName())
	}
	return request
}

func (c *Client) PostRequest() *rest.Request {
	return c.REST.Post().
		Resource(c.Resource.GetType()).
		SubResource(c.Resource.GetSubType()).
		Body(c.getBody())
}

func (c *Client) PutRequest() *rest.Request {
	return c.REST.Put().
		Resource(c.Resource.GetType()).
		Name(c.Resource.GetName()).
		SubResource(c.Resource.GetSubType()).
		Body(c.getBody())
}

func (c *Client) PatchRequest() *rest.Request {
	return c.REST.Patch(types.PatchType(c.Resource.GetPatchType())).
		Resource(c.Resource.GetType()).
		Name(c.Resource.GetName()).
		SubResource(c.Resource.GetSubType()).
		Body(c.getBody())
}
