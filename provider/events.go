package provider

import (
	"fmt"
	"time"

	"hedwig/cache"
	"hedwig/common"
	"hedwig/model"

	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	apiV1 "k8s.io/client-go/pkg/api/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type EventProvider struct {
	clientset *kubernetes.Clientset
	ch        chan *apiV1.Event
	stopped   bool
	failed    int
	cluster   *model.Cluster
	fetcher   *cache.Fetcher
	Logger    *common.Logger
}

func (p *EventProvider) String() string {
	return fmt.Sprintf("{\n  \"cluster\": \"%s/%s\",\n  \"stopped\": %v,\n  \"failed\": %d\n}\n", p.cluster.Name, p.cluster.UUID, p.stopped, p.failed)
}

func (p *EventProvider) Stop() {
	if p.stopped {
		return
	}
	p.stopped = true
	p.Logger.Infof("Event provider stopped")
}

func (p *EventProvider) ResultChan() <-chan *apiV1.Event {
	return p.ch
}

func (p *EventProvider) loop() {
	defer close(p.ch)
	var err error
	var watcher watch.Interface
	for !p.stopped {
		watcher, err = p.clientset.CoreV1().Events("").Watch(metaV1.ListOptions{Watch: true})
		if err != nil {
			p.Logger.Errorf("Watch cluster failed, %s", err.Error())
			p.update()
			continue
		}
		for watchEvent := range watcher.ResultChan() {
			apiEvent, ok := watchEvent.Object.(*apiV1.Event)
			if !ok {
				continue
			}
			p.Logger.Infof("Get event: %s/%s/%s %s", apiEvent.InvolvedObject.Namespace, apiEvent.InvolvedObject.Name, apiEvent.InvolvedObject.Kind, apiEvent.Reason)
			p.ch <- apiEvent
			p.failed = 0
		}
		watcher.Stop()
		p.update()
	}
}

func (p *EventProvider) update() {
	p.failed += 1
	cluster := p.fetcher.Get(p.cluster.UUID)
	if cluster == nil {
		p.stopped = true
		p.Logger.Infof("Update cluster for provider, provider will quit")
		return
	}
	p.cluster = cluster
	p.clientset = kubernetes.NewForConfigOrDie(cluster.GenerateRestConfig())
	p.Logger.Infof("Update cluster for provider, provider will sleep %d seconds", time.Duration(30*p.failed))
	time.Sleep(time.Second * time.Duration(30*p.failed))
}

func New(cluster *model.Cluster, fetcher *cache.Fetcher) (*EventProvider, error) {
	provider := &EventProvider{
		clientset: kubernetes.NewForConfigOrDie(cluster.GenerateRestConfig()),
		ch:        make(chan *apiV1.Event),
		failed:    0,
		cluster:   cluster,
		fetcher:   fetcher,
		Logger:    common.NewLogger(map[string]string{"cluster": cluster.Name + "/" + cluster.UUID}),
	}
	go provider.loop()
	return provider, nil
}
