package cache

import (
	"sync"
	"time"

	"hedwig/common"
	"hedwig/config"
	"hedwig/model"

	"github.com/deckarep/golang-set"
)

var fetcher *Fetcher
var globalClusterName string

type Fetcher struct {
	lock     *sync.RWMutex
	storage  Storage
	clusters map[string]*model.Cluster
	Logger   *common.Logger
}

func (f *Fetcher) Keys() []string {
	f.lock.RLock()
	defer f.lock.RUnlock()
	var keys []string
	for key := range f.clusters {
		keys = append(keys, key)
	}
	return keys
}

func (f *Fetcher) Get(k string) *model.Cluster {
	f.lock.RLock()
	defer f.lock.RUnlock()
	if val, ok := f.clusters[k]; ok {
		return val.Clone()
	}
	return nil
}

func (f *Fetcher) Set(k string, v *model.Cluster) {
	f.lock.Lock()
	defer f.lock.Unlock()
	f.clusters[k] = v
}

func (f *Fetcher) Delete(k string) {
	f.lock.Lock()
	defer f.lock.Unlock()
	delete(f.clusters, k)
}

func (f *Fetcher) Watch() {
	for {
		// List clusters
		clusters, err := f.storage.List()
		if err != nil {
			f.Logger.Errorf("List clusters error, sleep 15s, %v", err.Error())
			time.Sleep(time.Second * 15)
			continue
		}

		// Parse cluster features
		if err = f.storage.Parse(clusters); err != nil {
			f.Logger.Errorf("Parse cluster error, sleep 15s, %v", err.Error())
			time.Sleep(time.Second * 15)
			continue
		}

		// Update clusters set
		f.updateFetcherClusters(clusters)
		time.Sleep(time.Second * time.Duration(config.GlobalConfig.Furion.SyncPeriod))
	}
}

func (f *Fetcher) updateFetcherClusters(clusters map[string]*model.Cluster) {
	current := mapset.NewSet()
	previous := mapset.NewSet()
	for _, cluster := range clusters {
		current.Add(cluster.UUID)
	}
	for _, key := range f.Keys() {
		previous.Add(key)
	}
	insertSet := current.Difference(previous)
	updateSet := current.Intersect(previous)
	deleteSet := previous.Difference(current)
	for _, cluster := range clusters {
		if insertSet.Contains(cluster.UUID) {
			f.Logger.Infof("Insert cluster %s/%s", cluster.Name, cluster.UUID)
			cluster.CreatedAt = time.Now().UTC()
			cluster.UpdatedAt = time.Now().UTC()
			f.Set(cluster.UUID, cluster)
		}
		if updateSet.Contains(cluster.UUID) {
			old := f.Get(cluster.UUID)
			if old != nil && old.Equal(cluster) {
				continue
			}
			cluster.UpdatedAt = time.Now().UTC()
			f.Logger.Infof("Update cluster %s/%s", cluster.Name, cluster.UUID)
			f.Set(cluster.UUID, cluster)
		}
	}
	for _, key := range f.Keys() {
		if deleteSet.Contains(key) {
			region := f.Get(key)
			f.Logger.Infof("Delete cluster %s/%s", region.Name, region.UUID)
			f.Delete(key)
		}
	}
}
