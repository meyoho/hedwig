package config

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var BindEnv = map[string]string{
	"MORGANS_ENDPOINT":          "morgans_endpoint",
	"EVENT_CALLBACK_URL":        "event_callback_url",
	"MORGANS_TIMEOUT":           "morgans_timeout",
	"FURION_ENDPOINT":           "furion_endpoint",
	"FURION_APIVERSION":         "furion_apiversion",
	"FURION_TIMEOUT":            "furion_timeout",
	"FURION_CHECK_INTERVAL":     "furion_check_interval",
	"KUBERNETES_TIMEOUT":        "kubernetes_timeout",
	"MAX_EVENT_COUNT":           "max_event_count",
	"MAX_EVENT_INTERVAL":        "max_event_interval",
	"MAX_REPORT_RETRY":          "max_report_retry",
	"MAX_EVENT_LATENCY_SECONDS": "max_event_latency_seconds",
	"LOG_TO_STDOUT":             "log_to_stdout",
	"LOG_LEVEL":                 "log_level",
	"LOG_SIZE":                  "log_size",
	"LOG_BACKUP":                "log_backup",
}

var Defaults = map[string]interface{}{
	"max_event_count":           1000,
	"max_event_interval":        5,
	"max_report_retry":          3,
	"max_event_latency_seconds": 60,
	"morgans_timeout":           5,
	"furion_timeout":            5,
	"furion_check_interval":     60,
	"kubernetes_timeout":        600,
	"log_to_stdout":             false,
	"log_level":                 "info",
	"log_size":                  100,
	"log_backup":                2,
}

var Required = []string{
	"morgans_endpoint",
	"event_callback_url",
	"furion_endpoint",
	"furion_apiversion",
}

var config map[string]string
var initialized bool

func Initialize() error {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		logrus.Warning(err)
	}
	for key, val := range Defaults {
		viper.SetDefault(key, val)
	}

	for env, cfg := range BindEnv {
		err := viper.BindEnv(cfg, env)
		if err != nil {
			logrus.Error(err)
			return err
		}
	}

	for _, key := range Required {
		if !viper.IsSet(key) {
			err := fmt.Errorf("config %s is required", key)
			logrus.Error(err)
			return err
		}
	}

	initialized = true

	for _, key := range BindEnv {
		logrus.Infof("Env %s:%s", key, Get(key))
	}
	return nil
}

func Get(key string) string {
	if !initialized {
		panic("MUST call Initialize() first")
	}
	return viper.GetString(key)
}

func GetInt(key string) int {
	if !initialized {
		panic("MUST call Initialize() first")
	}
	return viper.GetInt(key)
}

func GetBool(key string) bool {
	if !initialized {
		panic("MUST call Initialize() first")
	}
	return viper.GetBool(key)
}
