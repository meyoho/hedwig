package config

import (
	"io/ioutil"

	"github.com/vrischmann/envconfig"
)

var (
	GlobalConfig Config
)

type Config struct {
	Davion struct {
		Endpoint   string `envconfig:"default=http://davion:80"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=10"`
		Enabled    bool   `envconfig:"default=true"`
	}

	Erebus struct {
		Endpoint string `envconfig:"default=https://erebus"`
	}

	Furion struct {
		Endpoint   string `envconfig:"default=http://furion:8080"`
		ApiVersion string `envconfig:"default=v2"`
		Timeout    int    `envconfig:"default=10"`
		SyncPeriod int    `envconfig:"default=300"`
	}

	Hedwig struct {
		CRDEnabled bool   `envconfig:"default=false"`
		Debug      bool   `envconfig:"default=true"`
		ExposePort int    `envconfig:"default=8080"`
		PodName    string `envconfig:"POD_NAME,default=hedwig-5dcbff4f6f-mlhgc"`
		Namespace  string `envconfig:"NAMESPACE,default=alauda-system"`
	}

	Kubernetes struct {
		Endpoint     string `envconfig:"default=https://kubernetes.default.svc.cluster.local"`
		Token        string `envconfig:"default=token"`
		Timeout      int    `envconfig:"default=10"`
		WatchTimeout int    `envconfig:"default=600"`
	}

	Log struct {
		Size     int    `envconfig:"default=100"` // unit: M
		Level    string `envconfig:"default=info"`
		Backup   int    `envconfig:"default=2"`
		ToStdout bool   `envconfig:"default=false"`
	}

	Prometheus struct {
		Namespace string `envconfig:"default=alauda-system"`
		Timeout   int    `envconfig:"default=10"`
	}
}

func init() {
	var tokenFile = "/var/run/secrets/kubernetes.io/serviceaccount/token"

	if err := envconfig.Init(&GlobalConfig); err != nil {
		panic("Load config from env error," + err.Error())
	}

	if token, err := ioutil.ReadFile(tokenFile); err == nil {
		GlobalConfig.Kubernetes.Token = string(token)
	}
}
